import java.util.Scanner;

public class Matrix {
    public static void main(String[] args){
        Scanner in =new Scanner(System.in);
        System.out.println("Enter the number of rows:");
        int rows = in.nextInt();
        System.out.println("Enter the number of columns:");
        int cols = in.nextInt();
        int[][] m1 = new int[rows][cols];
        int[][] m2 = new int[rows][cols];
        int[][] sum = new int[rows][cols];
        int[][] multiply = new int[rows][cols];
        System.out.println("Enter m1 data:");
        for(int i=0;i<rows;i++)
        {
            for(int j=0;j<cols;j++) {
               m1[i][j] = in.nextInt();
            }
        }
        for(int i=0;i<rows;i++)
        {
            for(int j=0;j<cols;j++){
                System.out.print(m1[i][j]+"\t");
            }
            System.out.println();
        }
        System.out.println("Enter m2 data:");
        for(int i=0;i<rows;i++)
        {
            for(int j=0;j<cols;j++) {
                m2[i][j] = in.nextInt();
            }
        }
        for(int i=0;i<rows;i++)
        {
            for(int j=0;j<cols;j++){
                System.out.print(m2[i][j]+"\t");
            }
            System.out.println();
        }
        for(int i=0;i<rows;i++)
        {
            for(int j=0;j<cols;j++){
                sum[i][j] = m1[i][j]+m2[i][j];
            }
        }
        System.out.println("Adding 2 matrices:");
        for(int i=0;i<rows;i++)
        {
            for(int j=0;j<cols;j++){
                System.out.print(sum[i][j]+"\t");
            }
            System.out.println();
        }
        System.out.println("Multiplying 2 matrices");
        for(int i=0;i<rows;i++){
            for(int j=0;j<cols;j++){
               for(int k=0;k<rows;k++){
                   multiply[i][j] += m1[i][k]*m2[k][j];
               }
            }
        }
        for(int i=0;i<rows;i++)
        {
            for(int j=0;j<cols;j++){
                System.out.print(multiply[i][j]+"\t");
            }
            System.out.println();
        }
    }
}
